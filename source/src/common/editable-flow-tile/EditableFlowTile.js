import React, {useState, useEffect} from 'react';
import './editable-flow-tile.scss';

function EditableFlowTile(props) {
    const [task, setTask] = useState({id:props.data.id, name: props.data.name, description: props.data.description, status: props.data.status});
    
    const STATUS_PENDING = 'PENDING';
    const STATUS_INPROGRESS = 'INPROGRESS';
    const STATUS_COMPLETED = 'COMPLETED';
    const STATUS = [STATUS_PENDING, STATUS_INPROGRESS, STATUS_COMPLETED];

    const localObjectChanger = function(flag, dataObj) {
        /* Haven't implemented empty checks because of time-constraint */
        switch(flag) {
          case 'TASK_NAME':
            setTask({...task, name: dataObj.target.value});
            break;
          case 'TASK_DESCRIPTION':     
            setTask({...task, description: dataObj.target.value});
            break;
          case 'TASK_STATUS':     
            setTask({...task, status: dataObj});
            break;
        }
      };

      useEffect(() => {
        props.updatedData(task);          
      }, [task]);

    const getStatusClass = function() {
      return (props.data.status===STATUS[0]?' pending': (props.data.status===STATUS[1]?' progress':'' ));  
    };

    const changeStatus = function() {
        let index = STATUS.indexOf(task.status);
        ++index;
        if(index>=STATUS.length) {
            index = 0;
        }
        localObjectChanger('TASK_STATUS', STATUS[index]);
    }

    const getUIElement= function() {
        const tile = 
        (<div className='editable-flow-tile'>       
            <div className='card-padding'>
            <div className='title'>              
                <input type='text' placeholder='Task name' onChange={(e)=>{localObjectChanger('TASK_NAME', e)}} />
            </div> 
            <div onClick={()=>{changeStatus()}} className={'status-change btn' + (getStatusClass())}>
                <i className='fa fa-check'></i>
            </div>
            <div className='description'>
                <textarea rows='6' placeholder='Describe the task' onChange={(e)=>{localObjectChanger('TASK_DESCRIPTION', e)}}></textarea>           
            </div>
            </div>
            {props.index%3!==2?
            <div className='arrow'>
                <div className='line'></div>
                <i className='fa fa-caret-right'></i>
                <i className='fa fa-caret-down'></i>
            </div>:''}
        </div>);
        const connection = (<div className='editable-flow-tile connection' key={props.index}>
                                <div className='left-dir'>
                                    <div className='line'></div>
                                    <i className='fa fa-caret-down'></i>
                                </div>            
                                <div className='connector'>
                                    <div className='line'></div>
                                </div>
                                <div className='right-dir'>
                                    <div className='line'></div>
                                </div>  
                            </div>);
        return (props.index>2 && props.index%3===0)?[connection, tile]:tile;
    };
    return getUIElement();
}

export default EditableFlowTile;
