import React from 'react';
import { connect } from 'react-redux';
import {modifyWorkflow, removeWorkflow} from './../../actions';
import './flow-tile.scss';

function FlowTile({data, dispatch}) {
    
const STATUS_PENDING = 'PENDING';
const STATUS_COMPLETED = 'COMPLETED';
const STATUS = [STATUS_PENDING, STATUS_COMPLETED];

const TASK_STATUS_PENDING = 'PENDING';
const TASK_STATUS_INPROGRESS = 'INPROGRESS';

const getStatusClass = function() {
    return (data.status===STATUS[0]?' pending': '');  
};

const changeStatus = function() {
    // Condition to check status pending
    let errFlag = false;
    data.tasks.forEach(task => {
        if(task.status === TASK_STATUS_PENDING || task.status === TASK_STATUS_INPROGRESS) {
            errFlag = true;
        }
    });
    if(!errFlag && data.tasks.length>0) {
        const dataObj = {
            id: data.id,
            name: data.name,
            status: (data.status===STATUS_COMPLETED?STATUS_PENDING:STATUS_COMPLETED),
            tasks: data.tasks
        };
        dispatch(modifyWorkflow(dataObj));
    }
}

const removeWorkFlow = function() {
    const dataObj = {
        id: data.id,        
      };
    dispatch(removeWorkflow(dataObj));
}

  return (
    <div className='flow-tile'>
        <div className='card-padding'>
        <div className='title'>              
            <input type='text' readOnly placeholder='Title' value={data.name} />
        </div> 
        <div className='delete btn' onClick={()=>removeWorkFlow()}>
            <i className='fa fa-trash-o'></i>
        </div>
        <div className='status-fields'>
            <div className='status'>{data.status}</div>
            <div onClick={()=>changeStatus()} className={'status-change btn' + (getStatusClass())}>
                <i className='fa fa-check'></i>
            </div>
        </div>
        </div>
    </div>
  );
}

export default connect()(FlowTile);
