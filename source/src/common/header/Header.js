import React from 'react';
import { withRouter } from 'react-router-dom';
import logo from './../../graphics/icons/app-icon-white.png';
import './header.scss';

function Header(props) {
  const moveToHome = function() {
    props.history.push("/home");
  };
  const doLogout = function() {
    /* Implement actual logout functionality when connected with a back-end. 
    For now, redirecting to login screen */
    props.history.push("/login");
  };
  return (
    <header className='header'>
      <div className='logo btn' onClick={()=>{moveToHome()}}>
        <img src={logo} alt='FlowApp' />
        <span>FLOWAPP</span>
      </div>
      <div className='action'>
        <div className='logout btn' onClick={()=>{doLogout()}}>
          <input className='btn' type='button' value='LOGOUT' />
        </div>
      </div>
    </header>
  );
}

export default withRouter(Header);