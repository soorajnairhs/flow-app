import React from 'react';
import { withRouter } from 'react-router-dom';
import './filter.scss';

function Filter(props) {
    const addWorkflow = function() {
        props.history.push("/workflow");
    };
  return (
    <div className='filter-container'>
        <div className='filter'>
            <div className='input-box'>
                <i className='fa fa-search' />
                <input type='text' placeholder='Search Workflows' />
            </div>
            <div className='select-box btn'>
                <select title='Filter'>
                    <option>ALL</option>
                    <option>COMPLETED</option>
                    <option>PENDING</option>
                </select>
                <i className='fa fa-filter' />
            </div>
        </div>
        <div className='create btn' title='Create a new workflow' onClick={()=>{addWorkflow()}}>
            <i className='fa fa-plus' />
            CREATE WORKFLOW
        </div>
    </div>
  );
}

export default withRouter(Filter);
