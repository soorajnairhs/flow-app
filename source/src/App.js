import React, {useLayoutEffect, useState} from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Header from './common/header/Header';
import Login from './components/login/Login';
import Home from './components/home/Home';
import Workflow from './components/workflow/Workflow';
import './App.scss';

function App() {
  
  const [size, setSize] = useState([0, 0]);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return (
    <div className={'app' + (size[0]<=900?' mobile':'')}>
      <BrowserRouter>
        <Header></Header>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/home' component={Home}/>
          <Route path='/login' component={Login}/>
          <Route path='/workflow' component={Workflow}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
