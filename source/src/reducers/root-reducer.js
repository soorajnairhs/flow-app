import { combineReducers } from 'redux';
import workFlows from './work-flows';
import workFlowFilters from './work-flow-filters';

export default combineReducers({
    workFlows,
    workFlowFilters
});
