const workFlows = (state = [], action) => {
    switch (action.type) {
      case 'ADD_WORKFLOW':
        return [
          ...state,
          {
            id: action.id,
            name: action.name,
            status: 'PENDING',
            tasks: action.tasks
          }
        ]
      case 'MODIFY_WORKFLOW':
        return state.map(workFlow =>
          (workFlow.id === action.id)
            ? {...workFlow, name: action.name, status: action.status, tasks:action.tasks}
            : workFlow
        )      
      case 'REMOVE_WORKFLOW':
        return state.filter((workFlow) => {
          if(workFlow.id !== action.id) {
            return workFlow;
          }}
        )
      default:
        return state
    }
  }
  
  export default workFlows;
  