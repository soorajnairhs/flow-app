import { workFlowFilters } from '../actions';

const workFlowFiltersObj = (state = workFlowFilters.SHOW_INPROGRESS, action) => {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter
    default:
      return state
  }
}

export default workFlowFiltersObj;
