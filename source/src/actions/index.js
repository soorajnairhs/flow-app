let nextWorkFlowID = 0;
export const addWorkflow = (data) => ({
  type: 'ADD_WORKFLOW',
  id: ++nextWorkFlowID,
  name: data.name,
  status: data.status,
  tasks: data.tasks
});

export const modifyWorkflow = (data) => ({
    type: 'MODIFY_WORKFLOW',
    id: data.id,
    name: data.name,
    status: data.status,
    tasks: data.tasks
});

export const removeWorkflow = (data) => ({
    type: 'REMOVE_WORKFLOW',
    id: data.id
});

export const workFlowFilters = {
  SHOW_ALL: 'ALL',
  SHOW_COMPLETED: 'COMPLETED',
  SHOW_INPROGRESS: 'INPROGRESS',
  SHOW_PENDING: 'PENDING'
}
