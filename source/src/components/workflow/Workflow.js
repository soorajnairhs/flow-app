import React, {useState} from 'react';
import { connect } from 'react-redux';
import { useHistory } from "react-router-dom";
import EditableFlowTile from './../../common/editable-flow-tile/EditableFlowTile';
import {addWorkflow} from './../../actions';
import './workflow.scss';

function Workflow({ dispatch }) {
  
  let history = useHistory();
  const [workFlow, setWorkFlow] = useState({id:'', name: '', status: 'PENDING'});
  const [tasks, setTasks] = useState([]);
  
  const TASK_STATUS_PENDING = 'PENDING';
  const TASK_STATUS_INPROGRESS = 'INPROGRESS';

  const addOrEditWorkflow = function(workflowObj) {
    const data = {
            name: workFlow.name,
            status: workFlow.status,
            tasks
          };
    dispatch(addWorkflow(data));
    history.push("/home");
  };

  const updatedTileData = function(data) {    
    localObjectChanger('TASK_UPDATE', data);
  }

  /* We can go with a UUID approach in production based code. Here id goes as an auto-inc. 
  Hence the below method is crucial, otherwise delete opertion could mess this up */
  const getId = function() {
    let id = 0;
    if(tasks.length>0) {
      tasks.sort((a, b)=>{ return b.id-a.id});
      id = tasks[0].id;
      ++id;
    }
    return id;
  }

  const shuffle = function() {
      let array = tasks;
      let shuffledArray = array.sort(() => Math.random() - 0.5);
      console.log(shuffledArray);
      setTasks([...shuffledArray]);
  }

  const localObjectChanger = function(flag, dataObj) {
    switch(flag) {
      case 'WORK_NAME':
        setWorkFlow({...workFlow, name: dataObj.target.value});
        break;
      case 'WORK_TASK_NEW':
        setTasks(oldTasks => [...oldTasks, {id: getId(), status: 'PENDING', name: '', description: ''}]);
        break;
      case 'TASK_UPDATE':
        setTasks(tasks.map((task, item)=>{
          return (task.id===dataObj.id)?dataObj: task;
        }));
        break;
      case 'WORK_TASK_REMOVE':
        if(tasks.length>0) {  
          let taskUpdated = tasks;    
          taskUpdated.pop();
          setTasks([...taskUpdated]);
        }
        break;
    }
  };

  const shuffleEnabled = function() {
    let errFlag = false;
    tasks.forEach(task => {
        if(task.status === TASK_STATUS_PENDING || task.status === TASK_STATUS_INPROGRESS) {
            errFlag = true;
        }
    });
    return (!errFlag && tasks.length>0);
  }

  return (
    <div className="workflow-page">
        <div className='workflow-filter-actions'>
          <div className='filter'>
              <div className='input-box'>                 
                  <input type='text' placeholder='WORKFLOW NAME' onChange={(e)=>{localObjectChanger('WORK_NAME', e)}} />
              </div>
          </div>
          <div className='actions'>
            {shuffleEnabled()?
            <div className='button alt btn' title='Shuffle nodes' onClick={()=>shuffle()}>
                <i className='fa fa-random' />
                Shuffle
            </div>:''}
            <div className='button alarm btn' title='Delete this node' onClick={()=>localObjectChanger('WORK_TASK_REMOVE', null)}>
                <i className='fa fa-trash' />
                Delete
            </div>
            <div className='button positive btn' title='Add a new node' onClick={()=>localObjectChanger('WORK_TASK_NEW', null)}>
                <i className='fa fa-plus' />
                Add Node
            </div>
            <div className='button normal btn' title='Save workflow' onClick={()=>addOrEditWorkflow()}>
                <i className='fa fa-save' />
                Save
            </div>
          </div>
      </div>
      {(tasks && tasks.length>0)?
        <div className='cards'>
          {tasks.map((task, index) => 
            <EditableFlowTile updatedData={updatedTileData} data={task} key={task.id} index={index}></EditableFlowTile>
            )}
        </div>:<div className='empty-task'>
                  <div className='label'>Start by adding a task</div>
                  <div className='button positive btn' title='Add a new node' onClick={()=>localObjectChanger('WORK_TASK_NEW', null)}>
                    <i className='fa fa-plus' />
                    Add Node
                  </div>
              </div>}
    </div>
  );
}

export default connect()(Workflow);
