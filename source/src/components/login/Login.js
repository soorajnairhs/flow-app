import React from 'react';
import './login.scss';

function Login(props) {
  const submitLogin = function(e) {
    props.history.push("/home");
    e.preventDefault();
  }
  return (
    <div className='login-container'>
      <div className='login-box'>
        <div className='title'>Login</div>
        <form onSubmit={(e) => {submitLogin(e)}}>
          <div className='input-box'>
            <i className='fa fa-envelope-o' />
            <input type='text' placeholder='Email' />
          </div>
          <div className='input-box'>
            <i className='fa fa-asterisk' />
            <input type='password' placeholder='Password' />
          </div>       
          <div className='action'>
            <div className='remember'>
              <input className='checkbox btn' type='checkbox' id='remember' name='remember' />
              <label className='btn' htmlFor="remember">Remember me</label>
            </div>
          </div>        
          <div className='action'>
            <div className='login btn'>
              <input type='submit' value='Login' />
            </div>
          </div>  
        </form>        
        <div className='action'>
          <div className='create btn'>
            Don't have an account? Sign up here
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
