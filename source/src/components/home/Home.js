import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import Filter from './../../common/filter/Filter';
import FlowTile from './../../common/flow-tile/FlowTile';
import './home.scss';

function Home({ dispatch }) {
  let history = useHistory();
  
  const addWorkflow = function() {
    history.push("/workflow");
  }; 

  return (
    <div className="home-page">
      <Filter></Filter>
      {(workFlowsArr && workFlowsArr.length>0)?
        <div className='cards'>
          {workFlowsArr.map((workFlow, index) => 
            <FlowTile data={workFlow} key={index}></FlowTile>
          )}
        </div>:<div className='empty-workflow'>
                  <div className='label'>Start by adding a new workflow</div>
                  <div className='button positive btn' title='Add a new workflow' onClick={()=>addWorkflow()}>
                    <i className='fa fa-plus' />
                    Add Workflow
                  </div>
              </div>
      }
    </div>
  );
}

let workFlowsArr = [];
const mapStateToProps = state => {
  workFlowsArr = state.workFlows;
  return {workFlows: state.workFlows}
};

export default connect(mapStateToProps)(Home);
